<?php

function shadow_tables_page() {
  $output = '';

  $rows = array();
  $header = array(t('Table'), t('Base table'), t('Queries'), t('Status'));
  $sql = 'SELECT t.tid, t.db_name, t.base_table, t.ready, (
    SELECT COUNT(*)
    FROM {shadow_query} q
    WHERE q.tid = t.tid
  ) as queries
  FROM {shadow_table} t
  ORDER BY t.db_name';
  $res = db_query($sql, 100);
  while ($rec = db_fetch_array($res)) {
    $rows[] = array(
      l($rec['db_name'], 'admin/build/shadow/tables/' . $rec['tid']),
      check_plain($rec['base_table']),
      $rec['queries'],
      $rec['ready'] ? t('ready') : t('needs rebuilding'),
    );
  }
  $output .= theme('table', $header, $rows);
  $output .= theme('pager');

  return $output;
}

function shadow_table_actions_form(&$form_state, $tid) {
  $form = array();

  $form['#tid'] = $tid;

  $sql = 'SELECT tid, db_name, base_table FROM {shadow_table} WHERE tid = %d';
  $res = db_query($sql, $tid);
  if (!$table = db_fetch_object($res)) {
    drupal_not_found();
    return;
  }

  $columns = array();
  $sql = 'SELECT * FROM {shadow_column} WHERE tid = %d';
  $res = db_query($sql, $tid);
  while ($column = db_fetch_object($res)) {
    $columns[] = $column;
  }

  $filters = array();
  $sql = 'SELECT * FROM {shadow_filter} WHERE tid = %d';
  $res = db_query($sql, $tid);
  while ($filter = db_fetch_object($res)) {
    $filters[] = $filter;
  }

  $sql = 'SELECT COUNT(*) FROM {%s}';
  $rows = db_result(db_query($sql, $table->db_name));
  $base_rows = db_result(db_query($sql, $table->base_table));

  $form['info'] = array(
    '#value' => '<p>' . t('The table %db_name currently has %rows rows (%base_rows rows in base table).', array('%db_name' => $table->db_name, '%rows' => $rows, '%base_rows' => $base_rows)) . '</p>',
  );
  
  $rows = array();
  $res = pager_query('SELECT * FROM {%s}', 5, 0, NULL, $table->db_name);
  while ($rec = db_fetch_array($res)) {
    $header = array();
    $row = array();
    foreach ($rec as $name => $value) {
      if (!is_numeric($name)) {
        $header[] = $name;
        $row[] = is_null($value) ? '<em>NULL</em>' : check_plain(substr($value, 0, 32));
      }
    }
    $rows[] = $row;
  }
  if ($rows) {
    $form['table'] = array(
      '#value' => theme('table', $header, $rows) . theme('pager'),
    );
  }
  
  $form['rebuild'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild table'),
  );

  $form['delete'] = array(
    '#value' => l(t('Delete table'), "admin/build/shadow/tables/$tid/delete"),
  );

  return $form;
}

function shadow_table_actions_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] == 'edit-rebuild') {
    module_load_include('inc', 'shadow', 'shadow.index');
    shadow_index_add($form['#tid']);
    drupal_set_message(t('The table has been rebuilt.'));
    $form_state['redirect'] = 'admin/build/shadow';
  }
}

function shadow_table_delete_form(&$form_state, $tid) {
  $form = array();

  $sql = 'SELECT db_name, base_table FROM {shadow_table} WHERE tid = %d';
  $res = db_query($sql, $tid);
  if (!$table = db_fetch_object($res)) {
    drupal_not_found();
    return;
  }

  $form['#tid'] = $tid;
  $form['#db_name'] = $table->db_name;

  $form['info'] = array(
    '#value' => '<p>' . t('Are you sure you want to delete the table %db_name?', array('%db_name' => $table->db_name)) . '</p>',
  );

  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete table'),
  );

  $form['cancel'] = array(
    '#value' => l(t('Cancel'), "admin/build/shadow/tables/$tid"),
  );

  return $form;
}

function shadow_table_delete_form_submit($form, &$form_state) {
  $sql = 'UPDATE {shadow_query} SET tid = NULL WHERE tid = %d';
  db_query($sql, $form['#tid']);
  $sql = 'DELETE FROM {shadow_table} WHERE tid = %d';
  db_query($sql, $form['#tid']);
  $sql = 'DELETE FROM {shadow_column} WHERE tid = %d';
  db_query($sql, $form['#tid']);
  $sql = 'DELETE FROM {shadow_filter} WHERE tid = %d';
  db_query($sql, $form['#tid']);
  $sql = 'DROP TABLE {%s}';
  db_query($sql, $form['#db_name']);
  drupal_set_message(t('The table has been deleted.'));
  $form_state['redirect'] = 'admin/build/shadow';
}

function shadow_queries_page() {
  $output = '';

  $rows = array();
  $header = array(t('Guid'), t('Description'), t('Shadow table'), t('Changed'));
  $sql = 'SELECT q.qid, q.guid, q.description, q.query_changed, t.db_name
  FROM {shadow_query} q
  LEFT JOIN {shadow_table} t ON t.tid = q.tid
  ORDER BY q.guid';
  $res = pager_query($sql, 100);
  while ($rec = db_fetch_array($res)) {
    $rows[] = array(
      l($rec['guid'], 'admin/build/shadow/queries/list/' . $rec['qid']),
      check_plain($rec['description']),
      empty($rec['db_name']) ? '<em>' . t('not rewritten') . '</em>' : check_plain($rec['db_name']),
      format_date($rec['query_changed'], 'short'),
    );
  }
  $output .= theme('table', $header, $rows);
  $output .= theme('pager');
  
  return $output;
}

function shadow_add_query_form($form_state) {
  $form = array();

  $form['guid'] = array(
    '#type' => 'textfield',
    '#title' => t('Global identifier'),
    '#required' => TRUE,
    '#description' => t('Identifier for a query of this kind.'),
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Short description'),
    '#required' => TRUE,
    '#description' => t('This description is only used for display in the admin section.'),
  );

  $form['query'] = array(
    '#type' => 'textarea',
    '#title' => t('SQL code'),
    '#required' => TRUE,
    '#wysiwyg' => FALSE,
    '#description' => t('Enter the full SQL code while leaving all tokens, values and other decorations intact.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function shadow_add_query_form_validate($form, &$form_state) {
  $guid = $form_state['values']['guid'];
  $sql = 'SELECT q.qid FROM {shadow_query} q WHERE q.guid = \'%s\'';
  $res = db_query($sql, $guid);
  if (db_fetch_array($res)) {
    form_set_error('guid', t('The given global identifier is already in use.'));
  }

  _shadow_load_classes();
  $query = new ShadowQuery($form_state['values']['query']);
  $base_table = $query->getBaseTable();
  if (empty($base_table)) {
    form_set_error('query', t('Unable to read the base table from query. Please check if this is a valid SELECT query.'));
  }
  if (!_shadow_get_primary_key($base_table)) {
    form_set_error('query', t('The base table %table is not supported because it does not exists or does not have a primary key.', array('%table' => $base_table)));
  }
}

function shadow_add_query_form_submit($form, &$form_state) {
  $query = new stdClass();
  $query->guid = $form_state['values']['guid'];
  $query->description = $form_state['values']['description'];
  $query->query = $form_state['values']['query'];
  $query->query_changed = time();
  $query->last_use = 0;
  drupal_write_record('shadow_query', $query);

  drupal_set_message(t('The query has been saved.'));
  drupal_goto("admin/build/shadow/queries/list/$query->qid/new");
}

function shadow_choose_table_form($form_state, $qid) {
  $form = array();

  $sql = 'SELECT guid, description, query, tid FROM {shadow_query} WHERE qid = %d';
  $res = db_query($sql, $qid);
  if (!$query = db_fetch_object($res)) {
    drupal_not_found();
    exit;
  }

  $form['#qid'] = $qid;

  $form['query'] = array(
    '#type' => 'textarea',
    '#title' => t('Query'),
    '#disabled' => TRUE,
    '#value' => $query->query,
  );

  $options = array('<none>' => t('Do not rewrite this query'));
  $sql = 'SELECT tid, db_name FROM {shadow_table} ORDER BY db_name ASC';
  $res = db_query($sql);
  while ($rec = db_fetch_array($res)) {
    $options[$rec['tid']] = $rec['db_name'];
  }
  $options['<new>'] = t('Create a new table');
  $form['table'] = array(
    '#type' => 'radios',
    '#title' => t('Shadow table'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => empty($query->tid) ? '<none>' : $query->tid,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function shadow_choose_table_form_submit($form, &$form_state) {
  $qid = $form['#qid'];
  $table = $form_state['values']['table'];
  if ($table == '<new>') {
    $form_state['redirect'] = "admin/build/shadow/queries/list/$qid/new";
  }
  elseif ($table == '<none>') {
    $sql = 'UPDATE {shadow_query} SET tid = NULL WHERE qid = %d';
    db_query($sql, $qid);
    drupal_set_message(t('The settings has been saved.'));
    $form_state['redirect'] = 'admin/build/shadow/queries';
  }
  else {
    $sql = 'UPDATE {shadow_query} SET tid = %d WHERE qid = %d';
    db_query($sql, $table, $qid);
    drupal_set_message(t('The settings has been saved.'));
    $form_state['redirect'] = 'admin/build/shadow/queries';
  }
}

function shadow_create_table_form($form_state, $qid) {
  $form = array();

  $sql = 'SELECT query FROM {shadow_query} WHERE qid = %d';
  $res = db_query($sql, $qid);
  if (!$rec = db_fetch_array($res)) {
    drupal_not_found();
    exit;
  }
  $sql = $rec['query'];

  module_load_include('inc', 'shadow', 'classes/query');
  
  $query = new ShadowQuery($sql);

  $weight_options = array();
  for ($i = 0; $i <= 128; ++$i) {
    $weight_options[$i] = $i;
  }

  $primary_key = _shadow_get_primary_key($query->getBaseTable());
  
  $form['#base_table'] = $query->getBaseTable();
  $form['#primary_key'] = $primary_key;
  $form['#qid'] = $qid;
  
  $fields = $primary_key;
  $filters = array();
  foreach ($query->getFilterFields() as $definition) {
    $definition = explode(':', $definition, 2);
    $fields[] = $definition[0];
    if (isset($definition[1])) {
      $filters[] = $definition;
    }
  }
  $filters[] = '';
  $filters[] = '';

  $sortfields = $query->getSortingFields();
  $sortfields[] = '';
  $sortfields[] = '';

  $fields = array_unique($fields);
  $fields[] = '';
  $fields[] = '';
  
  $table_id = 1;
  while (db_table_exists("shadow_data_$table_id")) {
    ++$table_id;
  }
  $default_tablename = "shadow_data_$table_id";

  $form['db_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Tablename'),
    '#default_value' => $default_tablename,
    '#description' => t('Tablename used in database.'),
    '#required' => TRUE,
  );

  $form['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields to include in the table'),
    '#description' => t('These fields are included in the database table. Select the fields which are important for dynamic filtering.'),
    '#collapsible' => FALSE,
    '#theme' => 'shadow_fields',
    'items' => array(),
  );
  $c = 0;
  foreach ($fields as $field) {
    ++$c;
    $definition = $field;

    if (empty($field)) {
      $form['fields']['items'][] = array(
        "field$c" => array(
          '#type' => 'checkbox',
          '#title' => t('custom'),
          '#default_value' => TRUE,
          '#disabled' => TRUE,
        ),
        "field_name$c" => array(
          '#type' => 'textfield',
          '#title' => t('Name'),
          '#default_value' => _shadow_clean_name($field),
          '#description' => t('Columnname used in database'),
        ),
        "field_definition$c" => array(
          '#type' => 'textfield',
          '#title' => t('Definition'),
          '#maxlength' => 512,
          '#description' => t('Field definition, i.e.: %example', array('%example' => 'node.uid=users.uid,users.name')),
        ),
        "field_weight$c" => array(
          '#type' => 'select',
          '#title' => t('Weight'),
          '#options' => $weight_options,
          '#default_value' => $c,
          '#attributes' => array('class' => 'shadow-fields-weight'),
        ),
      );
    }
    else {
      $joins = explode(',', $field);
      $field = array_pop($joins);
      $joins = implode(', ', $joins);
      $joins = str_replace('*', ' (' . t('left join') . ')', $joins);
      $default_checked = in_array($field, $primary_key);
      list($field_table, $field_column) = explode('.', $field);
      $schema = drupal_get_schema($field_table);
      if (!is_array($schema) || !isset($schema['fields'][$field_column])) {
        if (!isset($warned)) {
          drupal_set_message(t('Not all fields are shown cause some of them are not found within the schema.'), 'warning');
        }
        $warned = TRUE;
        continue;
      }
      $form['fields']['items'][] = array(
        "field$c" => array(
          '#type' => 'checkbox',
          '#title' => check_plain($field),
          '#default_value' => $default_checked,
          '#disabled' => in_array($field, $primary_key),
        ),
        "field_info$c" => array(
          '#value' => $joins ? t('Joined:') . ' ' . check_plain($joins) : t('Available in base table'),
        ),
        "field_name$c" => array(
          '#type' => 'textfield',
          '#title' => t('Name'),
          '#default_value' => _shadow_clean_name($field),
          '#description' => t('Columnname used in database'),
        ),
        "field_definition$c" => array(
          '#type' => 'hidden',
          '#value' => $definition,
        ),
        "field_weight$c" => array(
          '#type' => 'select',
          '#title' => t('Weight'),
          '#options' => $weight_options,
          '#default_value' => $c,
          '#attributes' => array('class' => 'shadow-fields-weight'),
        ),
      );
    }
  }

  $form['sortfields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sortfields to include in the table'),
    '#description' => t('These fields are included in the database table. They can be used in the ORDER BY section of the query.'),
    '#collapsible' => FALSE,
    '#theme' => 'shadow_fields',
    'items' => array(),
  );
  $c = 0;
  foreach ($sortfields as $field) {
    ++$c;
    $definition = $field;

    if (empty($field)) {
      $item = array(
        "sortfield$c" => array(
          '#type' => 'checkbox',
          '#title' => t('custom'),
          '#default_value' => TRUE,
          '#disabled' => TRUE,
        ),
        "sortfield_name$c" => array(
          '#type' => 'textfield',
          '#title' => t('Name'),
          '#default_value' => '',
          '#description' => t('Columnname used in database'),
        ),
        "sortfield_definition$c" => array(
          '#type' => 'textfield',
          '#title' => t('Definition'),
          '#maxlength' => 512,
          '#description' => t('Field definition, i.e.: %example', array('%example' => 'node.uid=users.uid,users.name')),
        ),
        "sortfield_weight$c" => array(
          '#type' => 'select',
          '#title' => t('Weight'),
          '#options' => $weight_options,
          '#default_value' => $c,
          '#attributes' => array('class' => 'shadow-fields-weight'),
        ),
        "sortfield_invert$c" => array(
          '#type' => 'checkbox',
          '#title' => t('Invert value'),
          '#default_value' => FALSE,
        ),
        "sortfield_base$c" => array(
          '#type' => 'textfield',
          '#title' => t('Base'),
          '#default_value' => 2147483647,
        ),
      );
    }
    else {
      $definition = preg_replace('/\\*$/', '', $definition);
      $joins = explode(',', $field);
      $field = array_pop($joins);
      $joins = implode(', ', $joins);
      $joins = str_replace('*', ' (' . t('left join') . ')', $joins);
      $field_name = str_replace('*', '', $field);
      $default_base = _shadow_get_default_base($field_name);
      $default_invert = (strstr($field, '*') && $default_base);
      $item = array(
        "sortfield$c" => array(
          '#type' => 'checkbox',
          '#title' => check_plain($field_name),
        ),
        "sortfield_info$c" => array(
          '#value' => $joins ? t('Joined:') . ' ' . check_plain($joins) : t('Available in base table'),
        ),
        "sortfield_name$c" => array(
          '#type' => 'textfield',
          '#title' => t('Name'),
          '#default_value' => _shadow_clean_name($field_name),
          '#description' => t('Columnname used in database'),
        ),
        "sortfield_definition$c" => array(
          '#type' => 'hidden',
          '#value' => $definition,
        ),
        "sortfield_weight$c" => array(
          '#type' => 'select',
          '#title' => t('Weight'),
          '#options' => $weight_options,
          '#default_value' => $c,
          '#attributes' => array('class' => 'shadow-fields-weight'),
        ),
      );
      if ($default_base) {
        $item += array(
          "sortfield_invert$c" => array(
            '#type' => 'checkbox',
            '#title' => t('Invert value'),
            '#default_value' => $default_invert,
          ),
          "sortfield_base$c" => array(
            '#type' => 'textfield',
            '#title' => t('Base'),
            '#default_value' => $default_base,
          ),
        );
      }
    }
    $form['sortfields']['items'][] = $item;
  }

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enabled filters'),
    '#description' => t('Select the filters which apply to the shadow table. Take care when checking these filters, they may change the output of the query if this filters more rows than the original query.'),
    '#collapsible' => FALSE,
    '#theme' => 'shadow_fields',
    'items' => array(),
  );
  $c = 0;
  foreach ($filters as $field) {
    ++$c;
    if ($field) {
      $definition = implode(':', $field);
      $joins = explode(',', $field[0]);
      $field[0] = array_pop($joins);
      $joins = implode(', ', $joins);
      $joins = str_replace('*', ' (' . t('left join') . ')', $joins);
      $form['filters']['items'][] = array(
        "filter$c" => array(
          '#type' => 'checkbox',
          '#title' => check_plain($field[0]),
        ),
        "filter_info$c" => array(
          '#value' => ($joins ? t('Joined:') . ' ' . check_plain($joins) : t('Available in base table')) . '<br/>' .
            t('Allowed values:') . ' ' . check_plain($field[1]),
        ),
        "filter_definition$c" => array(
          '#type' => 'hidden',
          '#value' => $definition,
        ),
      );
    }
    else {
      $form['filters']['items'][] = array(
        "filter$c" => array(
          '#type' => 'checkbox',
          '#title' => t('custom'),
          '#default_value' => TRUE,
          '#disabled' => TRUE,
        ),
        "filter_definition$c" => array(
          '#type' => 'textfield',
          '#title' => 'definition',
          '#maxlength' => 512,
          '#description' => t('Field definition, i.e.: %example', array('%example' => 'node.type:\'page\',\'story\'')),
        ),
      );
    }
  }

  $form['build'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fill this table at creation'),
    '#default_value' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function shadow_create_table_form_validate($form, &$form_state) {
  _shadow_load_classes();
  $column_parser = new ShadowColumn();
  $filter_parser = new ShadowFilter();
  
  $db_name = $form_state['values']['db_name'];
  if (drupal_get_schema($db_name)) {
    form_set_error('db_name', t('A table with this name already exists.'));
  }
  elseif (!preg_match('/^[a-z][a-z0-9_]*$/', $db_name)) {
    form_set_error('db_name', t('Names may only contain alphanumeric characters and must start with a letter.'));
  }

  $names = array();
  $num_selected = 0;
  foreach ($form_state['values'] as $name => $value) {
    if (!preg_match('/^([a-z]+)(_[a-z]+)?([0-9]+)$/s', $name, $match)) {
      continue;
    }
    $field = $match[1] . $match[2];
    $element = $match[3];
    $definition = $form_state['values'][$match[1] . '_definition' . $element];
    $field_name = $form_state['values'][$match[1] . '_name' . $element];

    if (empty($definition)) {
      continue;
    }

    switch ($field) {
      case 'field_name':
      case 'sortfield_name':
        if (!empty($value)) {
          if (!preg_match('/^[a-z][a-z0-9_]*$/', $value)) {
            form_set_error($name, t('Names may only contain alphanumeric characters and must start with a letter.'));
          }
          elseif (in_array($value, $names)) {
            form_set_error($name, t('You may not use the same name twice.'));
          }
          $names[] = $value;
        }
        break;
      case 'sortfield_base':
        $max_value = _shadow_get_default_base($definition);
        if (!$max_value) {
          form_set_error($name, t('Invert base is not supported for this field.'));
        }
        else {
          if (!is_numeric($value)) {
            form_set_error($name, t('The base number must be a number.'));
          }
          elseif ($value < 0 || $value > $max_value) {
            form_set_error($name, t('The base number is out of range. The value must be between %min and %max.', array('%min' => 0, '%max' => $max_value)));
          }
        }
        break;
      case 'field':
      case 'sortfield':
        if ($value && !in_array($definition, $form['#primary_key']) && !empty($field_name)) {
          ++$num_selected;
        }
        break;
      case 'filter':
        if ($value) {
          ++$num_selected;
        }
        break;
      case 'field_definition':
      case 'sortfield_definition':
      case 'filter_definition':
        if (!empty($definition)) {
          try {
            if ($field == 'filter_definition') {
              $filter_parser->parse($definition);
            }
            else {
              $column_parser->parse($definition);
            }
          }
          catch (Exception $e) {
            form_set_error($name, t('Syntax error in definition: %error.', array('%error' => $e->getMessage())));
          }
        }
        break;
    }
  }

  if (!$num_selected) {
    form_set_error('', t('You must check at least one additional field or filter.'));
  }
}

function shadow_create_table_form_submit($form, &$form_state) {
  $db_name = $form_state['values']['db_name'];
  $columns = array();
  $filters = array();

  $table = new stdClass();
  $table->db_name = $db_name;
  $table->base_table = $form['#base_table'];
  $table->ready = 0;
  drupal_write_record('shadow_table', $table);

  foreach ($form_state['values'] as $name => $value) {
    if (!preg_match('/^([a-z]+)(_[a-z]+)?([0-9]+)$/s', $name, $match)) {
      continue;
    }
    $field = $match[1] . $match[2];
    $element = $match[3];
    $definition = $form_state['values'][$match[1] . '_definition' . $element];
    // Check if the checkbox is checked.
    if (empty($form_state['values'][$match[1] . $element])) {
      continue;
    }
    // Check if the definition is empty.
    if (empty($definition)) {
      continue;
    }
    switch ($field) {
      case 'field':
        if (empty($form_state['values']["field_name$element"])) {
          continue;
        }
        $name = $form_state['values']["field_name$element"];
        $definition = $form_state['values']["field_definition$element"];
        $weight = $form_state['values']["field_weight$element"];
        if (!isset($columns[$name])) {
          $columns[$name] = new stdClass();
          $columns[$name]->tid = $table->tid;
          $columns[$name]->db_name = $name;
          $columns[$name]->definition = $definition;
          $columns[$name]->weight = $weight;
        }
        $columns[$name]->index_filter = $weight;
        break;
      case 'sortfield':
        if (empty($form_state['values']["sortfield_name$element"])) {
          continue;
        }
        $name = $form_state['values']["sortfield_name$element"];
        $definition = $form_state['values']["sortfield_definition$element"];
        $weight = $form_state['values']["sortfield_weight$element"];
        if (isset($form_state['values']["sortfield_weight$element"])) {
          $invert = $form_state['values']["sortfield_invert$element"];
          $base = $form_state['values']["sortfield_base$element"];
        }
        if (!isset($columns[$name])) {
          $columns[$name] = new stdClass();
          $columns[$name]->tid = $table->tid;
          $columns[$name]->db_name = $name;
          $columns[$name]->definition = $definition;
          $columns[$name]->weight = $weight;
        }
        $columns[$name]->index_sort = $weight;
        if ($invert) {
          $columns[$name]->invert_base = $base;
        }
        break;
      case 'filter':
        $definition = $form_state['values']["filter_definition$element"];
        $filters[$name] = new stdClass();
        $filters[$name]->tid = $table->tid;
        $filters[$name]->definition = $definition;
        break;
    }
  }

  foreach ($columns as $column) {
    drupal_write_record('shadow_column', $column);
  }

  foreach ($filters as $filter) {
    drupal_write_record('shadow_filter', $filter);
  }

  usort($columns, '_shadow_sort_columns');

  module_load_include('inc', 'shadow', 'shadow.schema');
  $definition = shadow_schema_build_table($table, $columns, $filters);
  $sql = db_create_table_sql($table->db_name, $definition);
  db_query($sql[0]);

  $sql = 'UPDATE {shadow_query} SET tid = %d WHERE qid = %d';
  db_query($sql, $table->tid, $form['#qid']);

  if ($form_state['values']['build']) {
    usleep(10000); // Wait for INSERT INTO delays...
    module_load_include('inc', 'shadow', 'shadow.index');
    shadow_index_add($table->tid);
  }

  drupal_set_message(t('The new shadow table has been created.'));
  $form_state['redirect'] = 'admin/build/shadow';
}

function _shadow_sort_columns($a, $b) {
  if ($a->weight == $b->weight) {
    return 0;
  }
  return $a->weight < $b->weight ? -1 : 1;
}

function _shadow_get_default_base($field) {
  // Change field definition to fieldname.
  $field = explode(',', $field);
  $field = array_pop($field);
  $field = str_replace('*', '', $field);

  list($table, $column) = explode('.', $field);
  if (!$schema = drupal_get_schema($table)) {
    return FALSE;
  }
  if (!isset($schema['fields'][$column])) {
    return FALSE;
  }
  $type = $schema['fields'][$column]['type'];
  $unsigned = !empty($schema['fields'][$column]['unsigned']);
  $size = empty($schema['fields'][$column]['size']) ? 'normal' : $schema['fields'][$column]['size'];
  switch ($type) {
    case 'int':
    case 'serial':
      switch ($size) {
        case 'tiny': // 8 bits
          return $unsigned ? 255 : 127;
        case 'small': // 16 bits
          return $unsigned ? 65535 : 32767;
        case 'medium': // 32 bits
        case 'normal': // 32 bits
        default:
          return $unsigned ? 4294967295 : 2147483647;
        case 'big':
          return $unsigned ? 18446744073709551615 : 9223372036854775807;
      }
      break;
    case 'float':
      switch ($size) {
        case 'big':
          return $unsigned ? 4294967295 : 2147483647;
        default:
          return $unsigned ? 65535 : 32767;
      }
    default:
      return FALSE;
  }
}

function _shadow_clean_name($name) {
  $name = strtolower($name);
  $name = preg_replace('/[^a-z0-9]/', '_', $name);
  return $name;
}

/**
 * Theming function for the fields in the table builder form.
 *
 * @ingroup themeable
 */
function theme_shadow_fields($element) {
  static $counter = 0;
  ++$counter;
  $id = "shadow-fields-table-$counter";
  
  $header = array(
    t('Field'),
    t('Options'),
  );
  $rows = array();
  foreach ($element['items'] as $item_name => $item) {
    if ($item_name{0} == '#') {
      continue;
    }
    $checkbox = '';
    $options = '';
    $weight = '';
    $c = 0;
    foreach ($item as $name => $field) {
      if ($name{0} == '#') {
        continue;
      }
      ++$c;
      if ($c == 1) {
        $checkbox = drupal_render($field);
      }
      elseif (strstr($name, 'weight')) {
        $weight = drupal_render($field);
      }
      else {
        $options .= drupal_render($field);
      }
    }
    if ($weight) {
      $rows[] = array(
        'data' => array($checkbox, $options, $weight),
        'class' => 'draggable',
      );
    }
    else {
      $rows[] = array(
        'data' => array($checkbox, $options),
        'class' => 'draggable',
      );
    }
  }
  if ($weight) {
    drupal_add_tabledrag($id, 'weight', 'sibling', 'shadow-fields-weight');
    $header[] = t('Weight');
  }
  return theme('table', $header, $rows, array('id' => $id));
}

function shadow_queries_test_form(&$form_state) {
  $form = array();

  $form['query'] = array(
    '#type' => 'textarea',
    '#title' => t('SQL code'),
    '#required' => TRUE,
    '#wysiwyg' => FALSE,
    '#description' => t('Enter the full SQL code while leaving all tokens, values and other decorations intact.'),
  );

  $options = array();
  $sql = 'SELECT tid, db_name FROM {shadow_table} ORDER BY db_name ASC';
  $res = db_query($sql);
  while ($rec = db_fetch_array($res)) {
    $options[$rec['tid']] = $rec['db_name'];
  }
  $form['tables'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Shadow tables'),
    '#options' => $options,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function shadow_queries_test_form_submit($form, &$form_state) {
  $query = $form_state['values']['query'];
  $query_args = array();

  // Profile original query.
  timer_start('shadow');
  db_query($query, $query_args);
  $ms = timer_read('shadow');
  timer_stop('shadow');

  $header = array(t('Table'), t('Execution time (ms)'));
  $rows = array();
  $rows[] = array('<em>' . t('Original query') . '</em>', $ms);

  foreach ($form_state['values']['tables'] as $tid => $checked) {
    if (!$checked) {
      continue;
    }
    $sql = $query;
    $args = $query_args;
    timer_start("shadow$tid");
    db_query($sql, $args);
    $ms = timer_read("shadow$tid");
    timer_stop("shadow$tid");

    $rows[] = array($form['tables']['#options'][$tid], $ms);
  }
  
  drupal_set_message(theme('table', $header, $rows));
}
