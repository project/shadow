<?php

function shadow_schema_build_table($table, $columns, $filters) {
  $definition = array(
    'fields' => array(),
    'primary key' => array(),
    'indexes' => array(),
    'module' => 'shadow',
    'name' => $table->db_name,
  );

  $filter_index = array();
  $sort_index = array();

  // Strings used for comparing indices.
  $filter_index_str = '';
  $sort_index_str = '';

  usort($columns, '_shadow_schema_sort');

  if (!$base_table = drupal_get_schema($table->base_table)) {
    return FALSE;
  }

  _shadow_load_classes();
  
  foreach ($columns as $column) {
    $column_parser = new ShadowColumn($column->definition);
    
    $definition['fields'][$column->db_name] = $column_parser->getSchema();

    switch ($field_schema['type']) {
      case 'varchar':
        $index_length = max(16, $field_schema['length']);
        break;
      case 'text':
      case 'blob':
        $index_length = NULL;
        break;
      default:
        $index_length = FALSE;
        break;
    }
    if (!is_null($index_length)) {
      if ($column->index_filter) {
        // Check if field is not already available in primary key.
        if ($column_parser->getTableName() != $column->base_table || !in_array($column_parser->getFieldName(), $base_table['primary key'])) {
          $filter_index[] = $index_length ? array($column->db_name, $index_length) : $column->db_name;
          $filter_index_str .= '-' . $column->db_name;
        }
      }
      if ($column->index_sort) {
        $sort_index[] = $index_length ? array($column->db_name, $index_length) : $column->db_name;
        $sort_index_str .= '-' . $column->db_name;
      }
    }
    if (($column_parser->getTableName() == $table->base_table) && in_array($column_parser->getFieldName(), $base_table['primary key'])) {
      $definition['primary key'][] = $column->db_name;
    }
    elseif ($column_parser->isMultiple()) {
      $definition['primary key'][] = $column->db_name;
    }
  }
  
  if ($filter_index) {
    $definition['indexes']['filter_index'] = $filter_index;
  }
  if ($sort_index) {
    $definition['indexes']['sort_index'] = $sort_index;
  }

  if ($filter_index && $sort_index) {
    // Check if we may combine these indices.
    $length = min(strlen($filter_index_str), strlen($sort_index_str));
    if (substr($filter_index_str, 0, $length) == substr($sort_index_str, 0, $length)) {
      if ($length == strlen($filter_index_str)) {
        $definition['indexes'] = array('filter_sort_index' => $sort_index);
      }
      else {
        $definition['indexes'] = array('filter_sort_index' => $filter_index);
      }
    }
  }

  return $definition;
}

function _shadow_schema_sort($a, $b) {
  $a = max($a->index_filter, $a->index_sort);
  $b = max($b->index_filter, $b->index_sort);
  if ($a == $b) {
    return 0;
  }
  return $a < $b ? -1 : 1;
}