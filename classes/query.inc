<?php

class ShadowQuery {
  protected $sql;
  
  protected $tokens;
  protected $base_table;
  protected $base_table_alias;
  protected $joins;
  protected $filter_fields;
  protected $sort_fields;
  protected $field_aliases;
  protected $tables;
  protected $tables_alias;
  protected $filters;
  
  protected $combine = array(
    'INNER JOIN',
    'OUTER JOIN',
    'FULL JOIN',
    'FULL INNER JOIN',
    'FULL OUTER JOIN',
    'LEFT JOIN',
    'LEFT INNER JOIN',
    'LEFT OUTER JOIN',
    'RIGHT JOIN',
    'RIGHT INNER JOIN',
    'RIGHT OUTER JOIN',
    'ORDER BY',
    'GROUP BY',
  );

  protected $keywords = array(
    'SELECT', 'INSERT', 'ALTER', 'DELETE', 'CREATE', 'TABLE', 'FROM', 'WHERE',
    'JOIN', 'INNER', 'OUTER', 'LEFT', 'RIGHT', 'AS', 'LIMIT', 'OFFSET', 'ASC', 'DESC',
    'HAVING', 'ON', 'USING', 'COUNT', 'AVG', 'MIN', 'MAX', 'SUM', 'AND', 'OR', 'LIKE', 'IN',
    'COALESCE',
  );

  protected $aggregates = array(
    'COUNT', 'AVG', 'MIN', 'MAX', 'SUM',
  );

  protected $placeholders = array('s', 'n', 'd', 'f', 'b');

  protected $operators = array('=', '!=', '<>', '<', '>', '>=', '<=', 'LIKE');

  public function __construct($sql) {
    $this->sql = $sql;

    $this->tokens = array();
    $this->base_table = NULL;
    $this->base_table_alias = NULL;
    $this->joins = array();
    $this->filter_fields = array();
    $this->sort_fields = array();
    $this->field_aliases = array();
    $this->tables = array();
    $this->tables_alias = array();
    $this->filters = array();

    $this->tokenize();
    $this->parse();
    $this->analyseJoins();
    $this->prefixFields();
  }

  protected function tokenize() {
    $sql = $this->sql;

    // Filter out C-style comments.
    $sql = preg_replace("~('[^'\\\\]*(?:\\\\.[^'\\\\]*)*')|(?:(?:--|#).*|/\*(?:.|[\r\n])*?\*/)~", '$1', $sql);

    // Filter out SQL comments.
    $sql = preg_replace('/\\-\\-.*\\n/', '', $sql);

    $regex = '(?:([\'"])(?:\\\\\\\\|\\\\\\1|(?!\\1).|\\1\\1)*\1|(?:(?<!\\d)-)?\\d+(?:\\.\\d+)?(?:[eE]-?\\d+)?|\\.\\.|(?:\\w+\\.)*\\w+|[<>=|&]{2}|\\S)';
    $count = preg_match_all("/$regex/s", $sql, $matches);

    $this->tokens = array();
    for ($i = 0; $i < $count; ++$i) {
      // Define first, second and thirth tokens from current position in $f, $s and $t.
      $t1 = $matches[0][$i];
      $t2 = isset($matches[0][$i + 1]) ? $matches[0][$i + 1] : '';
      $t3 = isset($matches[0][$i + 2]) ? $matches[0][$i + 2] : '';
      // Combine tokens which consists of 3 words.
      if (in_array(strtoupper("$t1 $t2 $t3"), $this->combine)) {
        $this->tokens[] = strtoupper("$t1 $t2 $t3");
        $i += 2;
        continue;
      }
      // Combine tokens which consists of 2 words.
      if (in_array(strtoupper("$t1 $t2"), $this->combine)) {
        $this->tokens[] = strtoupper("$t1 $t2");
        ++$i;
        continue;
      }
      // Recognise placeholders.
      if ($t1 == '%' && in_array($t2, $this->placeholders)) {
        $this->tokens[] = "%$t2";
        ++$i;
        continue;
      }
      // Always add keywords in upper case to allow case sensitive matching.
      if (in_array(strtoupper($t1), $this->keywords)) {
        $this->tokens[] = strtoupper($t1);
        continue;
      }
      // Recognise table prefixing.
      if ($t1 == '{' && preg_match(SHADOW_REGEX_TABLENAME, $t2) && $t3 == '}') {
        $this->tokens[] = '{' . $t2 . '}';
        $i += 2;
        continue;
      }
      // Add the table.* syntax as a single token.
      if (preg_match(SHADOW_REGEX_TABLENAME, $t1) && $t2 == '.' && $t3 == '*') {
        $this->tokens[] = "$t1.*";
        $i += 2;
        continue;
      }
      // Add tokens used in views as a single token (i.e.: '***ADMINISTER_NODES***')
      if ($t1 == '*' && $t2 == '*' && $t3 == '*' && isset($matches[0][$i + 6])) {
        if ($matches[0][$i + 4] == '*' && $matches[0][$i + 5] == '*' && $matches[0][$i + 6] == '*') {
          $this->tokens[] = '***' . $matches[0][$i + 3] . '***';
          $i += 6;
          continue;
        }
      }
      if ($t1 == '&&') {
        $t1 = 'AND';
      }
      if ($t1 == '||') {
        $t1 = 'OR';
      }
      // Not a special token, add as-is.
      $this->tokens[] = $t1;
    }
  }

  protected function parse() {
    $level = 0;
    $section = '';
    $count = count($this->tokens);
    for ($i = 0; $i < $count; ++$i) {
      $t1 = $this->tokens[$i];
      $t2 = isset($this->tokens[$i + 1]) ? $this->tokens[$i + 1] : '';
      $t3 = isset($this->tokens[$i + 2]) ? $this->tokens[$i + 2] : '';
      $t4 = isset($this->tokens[$i + 3]) ? $this->tokens[$i + 3] : '';
      // Skip subqueries.
      if ($t1 == '(' && $t2 == 'SELECT') {
        $start_level = $level;
        ++$level;
        for ($i = ++$i; $i < $count; ++$i) {
          $t1 = $this->tokens[$i];
          if ($t1 == '(') {
            ++$level;
          }
          if ($t1 == ')') {
            --$level;
          }
          if ($level == $start_level) {
            continue(2);
          }
        }
      }
      // Update $section.
      if (in_array($t1, array('SELECT', 'FROM', 'WHERE', 'ORDER BY', 'GROUP BY', 'HAVING', 'LIMIT')) && $level == 0) {
        $section = $t1;
      }
      // Take out base table.
      if ($t1 == 'FROM' && $level == 0) {
        $this->base_table = preg_replace('/[\\{\\}]/', '', $t2);
        if (preg_match(SHADOW_REGEX_TABLENAME, $t3) && !in_array($t3, $this->keywords)) {
          $this->base_table_alias = $t3;
        }
        elseif ($t3 == 'AS') {
          $this->base_table_alias = $t4;
        }
        else {
          $this->base_table_alias = $t2;
        }
      }
      // Take out joins.
      if (preg_match('/^(JOIN|.* JOIN)$/si', $t1)) {
        $join_type = $t1;
        $join_table = $t2;
        $i += 2;
        $join_table_alias = NULL;
        if (preg_match(SHADOW_REGEX_TABLENAME, $t3) && !in_array($t3, $this->keywords)) {
          $join_table_alias = $t3;
          ++$i;
        }
        elseif ($t3 == 'AS') {
          $join_table_alias = $t4;
          $i += 2;
        }
        $t1 = isset($this->tokens[$i]) ? $this->tokens[$i] : '';
        $join_conditions = array();
        if ($t1 == 'USING') {
          // Using syntax is not supported.
          continue;
        }
        elseif ($t1 == 'ON') {
          $start_level = $level;
          for ($i = ++$i; $i < $count; ++$i) {
            $t1 = $this->tokens[$i];
            if ($t1 == '(') {
              ++$level;
            }
            if ($t1 == ')') {
              --$level;
            }
            if ($level == $start_level && preg_match('/(JOIN|WHERE|BY)$/si', $t1)) {
              break;
            }
            $join_conditions[] = $t1;
          }
          // The for loop consumers 1 token too much, seek back to prevent missing the next JOIN.
          --$i;
        }
        // Remove leading and trailing parentheses from $join_conditions if any.
        if ($join_conditions && $join_conditions[0] == '(' && $join_conditions[count($join_conditions) - 1] == ')') {
          array_shift($join_conditions);
          array_pop($join_conditions);
        }
        $this->joins[] = array(
          $join_type,
          $join_table,
          $join_table_alias,
          $join_conditions,
        );
      }
      // Take out field aliases
      if ($section == 'SELECT' && preg_match(SHADOW_REGEX_FIELDNAME, $t1) && $t2 == 'AS') {
        $this->field_aliases[$this->rawName($t3)] = $t1;
      }
      // Take out filtered fields
      if ($section == 'WHERE' && preg_match(SHADOW_REGEX_FIELDNAME, $t1) && (in_array($t2, $this->operators) || $t2 == 'IN')) {
        $filter_field = $t1;
        $filter_field_values = array();
        $filter_field_is_dynamic = FALSE;
        if (in_array($t2, $this->operators) && preg_match(SHADOW_REGEX_SCALAR, $t3)) {
          $filter_field_values[] = $t3;
        }
        elseif ($t2 == 'IN') {
          $start_level = $level;
          for ($i = $i + 2; $i < $count; ++$i) {
            $t1 = $this->tokens[$i];
            if ($t1 == '(') {
              ++$level;
            }
            if ($t1 == ')') {
              --$level;
            }
            if ($level == $start_level) {
              break;
            }
            if (preg_match(SHADOW_REGEX_SCALAR, $t1)) {
              $filter_field_values[] = $t1;
            }
            elseif ($t1 != '(' && $t1 != ')' && $t1 != ',') {
              $filter_field_is_dynamic = TRUE;
            }
          }
        }
        if (!$filter_field_is_dynamic && $filter_field_values) {
          sort($filter_field_values);
          $filter_field .= ':' . implode(',', $filter_field_values);
        }
        $this->filter_fields[] = $filter_field;
      }
      if ($section == 'WHERE' && $t1 == 'COALESCE') {
        $start_level = $level;
        for ($i = ++$i; $i < $count; ++$i) {
          $t1 = $this->tokens[$i];
          if ($t1 == '(') {
            ++$level;
          }
          if ($t1 == ')') {
            --$level;
          }
          if ($level == $start_level) {
            break;
          }
          if (preg_match(SHADOW_REGEX_FIELDNAME, $t1)) {
            $this->filter_fields[] = $t1;
          }
        }
      }
      // Take out sorting fields
      if ($section == 'ORDER BY' && preg_match(SHADOW_REGEX_FIELDNAME, $t1) && !in_array($t1, $this->keywords)) {
        $name = $this->rawName($t1);
        $name = isset($this->field_aliases[$name]) ? $this->field_aliases[$name] : $t1;
        $this->sort_fields[] = $name . ($t2 == 'DESC' ? '*' : '');
      }
    }
  }

  private function rawName($str) {
    return preg_replace('/[^a-z0-9_\\.]/si', '', $str);
  }

  protected function analyseJoins() {
    $this->tables = array();
    $this->tables_alias = array();
    if ($this->base_table_alias) {
      $this->tables[$this->rawName($this->base_table_alias)] = array($this->rawName($this->base_table), '', '');
      $this->tables_alias[$this->rawName($this->base_table)] = $this->rawName($this->base_table_alias);
    }
    foreach ($this->joins as $join) {
      $name = $this->rawName($join[2] ? $join[2] : $join[1]);
      $this->tables[$name] = array($this->rawName($join[1]), NULL);
      $this->tables_alias[$this->tables[$name][0]] = $name;
      if (!$join[3]) {
        // Joins without JOIN conditions (NATURAL JOIN and CROSS JOIN) are not supported.
        continue;
      }
      $conditions = array();
      $condition = array();
      foreach ($join[3] as $token) {
        if (preg_match('/^[\\\'0-9]/', $token) && !strstr($token, '%')) {
          // Scalar value without placeholders.
          $condition[] = $token;
        }
        elseif ($token == '=') {
          $condition[] = $token;
        }
        elseif ($token == 'AND') {
          $conditions[] = $condition;
          $condition = array();
        }
        elseif (preg_match(SHADOW_REGEX_FIELDNAME, $token)) {
          $token = explode('.', $this->rawName($token));
          $token[0] = isset($this->tables[$token[0]]) ? $this->tables[$token[0]][0] : $token[0];
          $condition[] = implode('.', $token);
        }
        else {
          // Unsupported condition (e.g. with OR, subqueries or placeholders).
          continue(2);
        }
      }
      $conditions[] = $condition;
      // Split out the conditions to references and filters.
      $references = array();
      $filters = array();
      foreach ($conditions as $condition) {
        if (count($condition) == 3) {
          if (preg_match(SHADOW_REGEX_FIELDNAME, $condition[0]) && preg_match(SHADOW_REGEX_FIELDNAME, $condition[2])) {
            // Reverse the comparisation if the first is the base table.
            if (preg_match('/^' . preg_quote($this->rawName($join[1])) . '\\./si', $condition[0])) {
              $references[] = $condition[2] . $condition[1] . $condition[0];
            }
            else {
              $references[] = $condition[0] . $condition[1] . $condition[2];
            }
            if ($join[0] == 'LEFT JOIN') {
              $references[0] .= '*';
            }
          }
          else {
            // Reverse the comparisation if the first is a scalar value.
            if (preg_match('/^[\\\'0-9]/s', $condition[0])) {
              $filters[] = $condition[2] . $condition[1] . $condition[0];
            }
            else {
              $filters[] = $condition[0] . $condition[1] . $condition[2];
            }
          }
        }
      }
      // Prefix references with references from left table.
      for ($i = 0; $i < count($references); ++$i) {
        $reference = explode('.', $references[$i]);
        if (isset($this->tables_alias[$reference[0]])) {
          $left_table = $this->tables_alias[$reference[0]];
          $references[$i] = (empty($this->tables[$left_table][1]) ? '' : $this->tables[$left_table][1] . ',') . $references[$i];
        }
      }
      sort($references);
      sort($filters);
      $this->tables[$name][1] = implode(',', $references);
      $this->tables[$name][2] = implode(',', $filters);
    }
  }
  
  protected function prefixFields() {
    // Prefix filter fields with references.
    for ($i = 0; $i < count($this->filter_fields); ++$i) {
      $reference = explode('.', $this->filter_fields[$i]);
      $original_reference = preg_replace('/^([^\\=]+)\\:.*$/', '\\1', $this->filter_fields[$i]);
      if (isset($this->tables[$reference[0]])) {
        $reference[0] = $this->tables[$reference[0]][0];
      }
      if (isset($this->tables_alias[$reference[0]])) {
        $left_table = $this->tables_alias[$reference[0]];
        $this->filter_fields[$i] = (empty($this->tables[$left_table][1]) ? '' : $this->tables[$left_table][1] . ',') . implode('.', $reference);
      }
      // Add the original reference. This is used when rewriting the query.
      $this->filter_fields[$i] .= "[$original_reference]";
    }

    // Prefix sorting fields with references.
    for ($i = 0; $i < count($this->sort_fields); ++$i) {
      $reference = explode('.', $this->sort_fields[$i]);
      $original_reference = preg_replace('/^([^\\=]+)\\:.*$/', '\\1', $this->sort_fields[$i]);
      if (isset($this->tables[$reference[0]])) {
        $reference[0] = $this->tables[$reference[0]][0];
      }
      if (isset($this->tables_alias[$reference[0]])) {
        $left_table = $this->tables_alias[$reference[0]];
        $this->sort_fields[$i] = (empty($this->tables[$left_table][1]) ? '' : $this->tables[$left_table][1] . ',') . implode('.', $reference);
      }
      // Add the original reference. This is used when rewriting the query.
      $this->sort_fields[$i] .= "[$original_reference]";
    }
  }

  public function reassembleQuery() {
    $sql = '';
    $prev_token = '';
    $level = 0;
    foreach ($this->tokens as $token) {
      if (in_array(strtoupper($token), $this->combine)) {
        $sql .= "\n" . str_repeat('  ', $level) . strtoupper($token);
      }
      elseif ($token == ')') {
        $sql .= ')';
        --$level;
      }
      elseif ($token == '(') {
        $sql .= $prev_token == '(' || in_array(strtoupper($prev_token), $this->aggregates) ? $token : " $token";
        ++$level;
      }
      elseif (strtoupper($token) == 'SELECT') {
        $sql .= 'SELECT';
      }
      elseif (in_array(strtoupper($token), $this->keywords)) {
        switch (strtoupper($token)) {
          case 'FROM':
          case 'JOIN':
          case 'WHERE':
          case 'LIMIT':
          case 'OFFSET':
          case 'HAVING':
          case 'AND':
          case 'OR':
            $sql .= "\n" . str_repeat('  ', $level) . strtoupper($token);
            break;
          default:
            $sql .= ' ' . strtoupper($token);
        }
      }
      else {
        $sql .= $prev_token == '(' ? $token : " $token";
      }
      $prev_token = $token;
    }
    return $sql;
  }

  public function inject($table, $primary_key, $filter_fields, $sort_fields, $runtime_filters) {
    
    // Generate an unique alias for the shadow table.
    $num = 1;
    $table_alias = 's';
    while (isset($this->tables[$table_alias]) || isset($this->tables_alias[$table_alias])) {
      ++$num;
      $table_alias = "s$num";
    }
    
    // Build the fields replacement table.
    $filter_fields_table = array();
    foreach ($filter_fields as $old_field => $new_field) {
      $alias = NULL;
      foreach ($this->filter_fields as $field) {
        if (preg_match('/^' . preg_quote($old_field) . '\\[(.*)\\]$/si', $field, $match)) {
          $filter_fields_table[$match[1]] = "$table_alias.$new_field";
        }
      }
    }
    
    $sort_fields_table = array();
    foreach ($sort_fields as $old_field => $new_field) {
      $alias = NULL;
      foreach ($this->sort_fields as $field) {
        if (preg_match('/^' . preg_quote($old_field) . '(\\*?)\\[(.*)\\]$/si', $field, $match)) {
          $sort_fields_table[$match[2]] = "$table_alias.$new_field" . $match[1];
          // Remove double inversion.
          $sort_fields_table[$match[2]] = str_replace('**', '', $sort_fields_table[$match[2]]);
        }
      }
    }
    
    // Build new token list.
    $tokens = array();
    $level = 0;
    $section = '';
    $runtime_filters_parenthesis_open = FALSE;
    $runtime_filters_added = FALSE;
    $count = count($this->tokens);
    
    for ($i = 0; $i < $count; ++$i) {
      $t1 = $this->tokens[$i];
      $t2 = isset($this->tokens[$i + 1]) ? $this->tokens[$i + 1] : '';
      $t3 = isset($this->tokens[$i + 2]) ? $this->tokens[$i + 2] : '';
      $t4 = isset($this->tokens[$i + 3]) ? $this->tokens[$i + 3] : '';
      
      // Skip subqueries.
      if ($t1 == '(' && $t2 == 'SELECT') {
        $start_level = $level;
        ++$level;
        for ($i = ++$i; $i < $count; ++$i) {
          $t1 = $this->tokens[$i];
          if ($t1 == '(') {
            ++$level;
          }
          if ($t1 == ')') {
            --$level;
          }
          if ($level == $start_level) {
            $tokens[] = $t1; // ??
            continue(2);
          }
        }
      }

      // Update $section.
      if (in_array($t1, array('SELECT', 'FROM', 'WHERE', 'ORDER BY', 'GROUP BY', 'HAVING', 'LIMIT')) && $level == 0) {
        $section = $t1;
      }

      // Replace base table and add join to original base table.
      if ($t1 == 'FROM' && $level == 0) {
        $tokens[] = 'FROM';
        $tokens[] = $table;
        $tokens[] = $table_alias;
        $tokens[] = 'JOIN';
        $tokens[] = $this->base_table;
        $tokens[] = $this->base_table_alias;
        $tokens[] = 'ON';
        $tokens[] = '(';
        $first = TRUE;
        foreach ($primary_key as $left_field => $right_field) {
          if (!$first) {
            $tokens[] = 'AND';
            $first = FALSE;
          }
          $tokens[] = "$table_alias.$left_field";
          $tokens[] = '=';
          $tokens[] = "$this->base_table_alias.$right_field";
        }
        $tokens[] = ')';

        if (preg_match(SHADOW_REGEX_TABLENAME, $t3) && !in_array($t3, $this->keywords)) {
          $i += 2;
        }
        elseif ($t3 == 'AS') {
          $i += 3;
        }
        else {
          $i += 1;
        }
        continue;
      }

      // Add runtime filters.
      if ($t1 == 'WHERE' && $level == 0 && $runtime_filters) {
        /**
         * @todo
         * The implementation of runtime filters is somewhat quircky.
         */
        $runtime_filters_parenthesis_open = TRUE;
        $runtime_filters_added = TRUE;
        $tokens[] = 'WHERE';
        $tokens[] = '(';
        foreach ($runtime_filters as $runtime_filter_number => $runtime_filter) {
          if ($runtime_filter_number > 0) {
            $tokens[] = 'AND';
          }
          $tokens[] = $table_alias . '.' . $runtime_filter->getFieldName();
          $tokens[] = '=';
          $tokens[] = "***RUNTIME_FILTER_$runtime_filter_number***";
        }
        $tokens[] = ')';
        $tokens[] = 'AND';
        $tokens[] = '(';
        continue;
      }
      if ($runtime_filters_parenthesis_open && in_array($t1, array('SELECT', 'FROM', 'WHERE', 'ORDER BY', 'GROUP BY', 'HAVING', 'LIMIT')) && $level == 0) {
        $runtime_filters_parenthesis_open = FALSE;
        $tokens[] = ')';
      }
      if (!$runtime_filters_added && in_array($t1, array('ORDER BY', 'GROUP BY', 'HAVING', 'LIMIT')) && $level == 0) {
        $runtime_filters_added = TRUE;
        $tokens[] = 'WHERE';
        foreach ($runtime_filters as $runtime_filter_number => $runtime_filter) {
          if ($runtime_filter_number > 0) {
            $tokens[] = 'AND';
          }
          $tokens[] = $table_alias . '.' . $runtime_filter->getFieldName();
          $tokens[] = '=';
          $tokens[] = "***RUNTIME_FILTER_$runtime_filter_number***";
        }
      }

      // Replace filter fields.
      if ($section == 'WHERE' && preg_match(SHADOW_REGEX_FIELDNAME, $t1)) {
        if (isset($filter_fields_table[$t1])) {
          $t1 = $filter_fields_table[$t1];
        }
      }

      // Replace sort fields.
      if ($section == 'ORDER BY' && preg_match(SHADOW_REGEX_FIELDNAME, $t1)) {
        $field = $t1;
        if (isset($this->field_aliases[$field])) {
          $field = $this->field_aliases[$field];
        }
        if ($t2 == 'DESC') {
          $field .= '*';
        }
        if (isset($sort_fields_table[$field])) {
          $tokens[] = str_replace('*', '', $sort_fields_table[$field]);
          if (strstr($sort_fields_table[$field], '*')) {
            $tokens[] = 'DESC';
          }
          if ($t2 == 'ASC' || $t2 == 'DESC') {
            ++$i;
          }
          continue;
        }
      }

      $tokens[] = $t1;
    }

    // Close parentheses with where added for the runtime filter if still open.
    if ($runtime_filters_parenthesis_open) {
      $runtime_filters_parenthesis_open = FALSE;
      $tokens[] = ')';
    }

    $this->tokens = $tokens;
  }

  public function getFilterFields() {
    $fields = array();
    foreach ($this->filter_fields as $field) {
      $field = preg_replace('/\\[.*\\]/', '', $field);
      in_array($field, $fields) or $fields[] = $field;
    }
    return $fields;
  }

  public function getSortingFields() {
    $fields = array();
    foreach ($this->sort_fields as $field) {
      $field = preg_replace('/\\[.*\\]/', '', $field);
      in_array($field, $fields) or $fields[] = $field;
    }
    return $fields;
  }

  public function getBaseTable() {
    return $this->base_table;
  }
}