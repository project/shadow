<?php

class ShadowStateMachine {
  protected $transitions;
  protected $state_holder;
  protected $stack_state_holder;

  /**
   * Constructor.
   *
   * @param array $transitions
   *   array(
   *     'start' => array(
   *       array('/./', 'source'),
   *     ),
   *     'source' => array(
   *       array('/[a-z]/', destination', 'token name', TRUE),
   *     ),
   *     'destination' => array(
   *       array('.', 'end'),
   *     ),
   *   )
   */
  public function __construct($transitions) {
    $this->transitions = $transitions;
    $this->state_holder = new ShadowStateHolder();
    $this->stack_state_holder = array();
  }

  /**
   * Parse input.
   *
   * @param string $input
   */
  public function parse($input) {
    $this->state_holder = new ShadowStateHolder();
    $this->stack_state_holder = array();
    $this->state_holder->state = 'start';
    $length = strlen($input);
    array_push($this->stack_state_holder, $this->state_holder);
    while (!empty($this->stack_state_holder)) {
      $this->state_holder = array_pop($this->stack_state_holder);
      // Check for end of input.
      if ($this->state_holder->position == $length) {
        if ($this->state_holder->state == 'end') {
          // Done.
          return;
        }
        else {
          // Check if there is a possible next end state.
          foreach ($this->transitions[$this->state_holder->state] as $transition) {
            $value = array_shift($transition);
            if (empty($value) && $transition[0] == 'end') {
              // Do a transition.
              if (!empty($transition[1])) {
                $this->state_holder->token_values[] = $this->state_holder->buffer;
                $this->state_holder->token_types[] = $transition[1];
              }
              $this->state_holder->state = $transition[0];
              // Done.
              return;
            }
          }
        }
        if (!empty($this->stack_state_holder)) {
          // Make another choice previously by popping from stack.
          continue;
        }
        else {
          throw new Exception("Unexpected end in state {$this->state_holder->state}", 2);
        }
      }
      $char = $input{$this->state_holder->position};
      // Loop backwards so that using the stack, the first possibility is popped first.
      foreach (array_reverse($this->transitions[$this->state_holder->state]) as $transition) {
        $value = array_shift($transition);
        if (strcmp($value, $char) == 0 || (strlen($value) > 1 && preg_match($value, $char))) {

          // Add possible next state to stack.
          $new_state = clone $this->state_holder;

          // Do a transition.
          if (!empty($transition[1])) {
            $new_state->token_values[] = $this->state_holder->buffer;
            $new_state->token_types[] = $transition[1];
            $new_state->buffer = '';
          }
          $new_state->state = $transition[0];
          if (!isset($this->transitions[$new_state->state])) {
            throw new Exception("Trying to switch to unknown state {$new_state->state}");
          }
          if (empty($transition[2])) {
            $new_state->buffer .= $char;
          }
          ++$new_state->position;
          array_push($this->stack_state_holder, $new_state);
        }
      }
    }
    if (empty($this->stack_state_holder)) {
      // No possible transition found!
      $near = substr($input, $this->state_holder->position, 10);
      throw new Exception("Parse error in state {$this->state_holder->state} at offset {$this->state_holder->position} (near $near)", 1);
    }
  }

  public function get_state_holder() {
    return $this->state_holder;
  }
}